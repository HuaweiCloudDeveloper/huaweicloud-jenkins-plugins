package com.huawei.jenkins.iam;

import hudson.EnvVars;
import hudson.Extension;
import hudson.FilePath;
import hudson.model.TaskListener;
import org.jenkinsci.plugins.workflow.steps.*;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class iamSecretStep extends Step {

    private String accessKey;

    private String secretKey;

    private String secretName;

    private String region;

    private String versionId;


    public String getAccessKey() {
        return accessKey;
    }

    @DataBoundSetter
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    @DataBoundSetter
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSecretName() {
        return secretName;
    }

    @DataBoundSetter
    public void setSecretName(String secretName) {
        this.secretName = secretName;
    }


    public String getRegion() {
        return region;
    }

    @DataBoundSetter
    public void setRegion(String region) {
        this.region = region;
    }

    public String getVersionId() {
        return versionId;
    }

    @DataBoundSetter
    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    @DataBoundConstructor
    public iamSecretStep(String region) {
        this.region = region;
    }

    @Override
    public StepExecution start(StepContext context) throws Exception {
        return new Execution(this, context);
    }


    @Extension
    public static class DescriptorImpl extends StepDescriptor {

        @Override
        public Set<? extends Class<?>> getRequiredContext() {
            return requires(TaskListener.class, EnvVars.class, FilePath.class);
        }

        @Override
        public String getFunctionName() {
            return "getContracts";
        }

        @Override
        public String getDisplayName() {
            return "华为云凭证获取";
        }
    }


    public static <T extends Class<?>> Set<T> requires(T... classes) {
        return new HashSet<>(Arrays.asList(classes));
    }


    public static class Execution extends SynchronousNonBlockingStepExecution<String> {

        protected static final long serialVersionUID = 1L;

        protected final transient iamSecretStep step;

        public Execution(iamSecretStep step, StepContext context) {
            super(context);
            this.step = step;

        }

        @Override
        public String run() throws Exception {
            CustomInput customInput = new CustomInput();
            customInput.setAccessKey(step.getAccessKey());
            customInput.setSecretKey(step.getSecretKey());
            customInput.setRegion(step.getRegion());
            customInput.setSecretName(step.getSecretName());
            customInput.setVersionId(step.getVersionId());
            TaskListener listener = iamSecretStep.Execution.this.getContext().get(TaskListener.class);
            return secretService.getCredentials(listener,customInput);
        }
    }


}
