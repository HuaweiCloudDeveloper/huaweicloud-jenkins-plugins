/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.jenkins.vss;

import hudson.EnvVars;
import hudson.Extension;
import hudson.FilePath;
import hudson.model.TaskListener;
import org.jenkinsci.plugins.workflow.steps.*;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * pipeline语法支持obs文件上传
 */
public class vssCheckPipelineStep extends Step {

    private String ak;

    private String sk;

    private String region;

    //是否将本次扫描升级为专业版规格(¥99.00/次)
    private String upgrade;

    //扫描任务的名称
    private String task_name;

    //被扫描的目标网址
    private String url;

    //扫描任务类型
    private String task_type;

    //普通任务的定时启动时间
    private String timer;

    //监测任务的定时触发时间
    private String trigger_time;

    //监测任务的定时触发周期
    private String task_period;

    //扫描模式:fast - 快速扫描 normal - 标准扫描 deep - 深度扫描
    private String scan_mode;

    //是否进行端口扫描
    private String port_scan;

    //是否进行弱密码扫描
    private String weak_pwd_scan;

    //是否进行CVE漏洞扫描
    private String cve_check;

    //是否进行网站内容合规文字检测
    private String text_check;

    //是否进行网站内容合规图片检测
    private String picture_check;

    //是否进行网站挂马检测
    private String malicious_code;

    //是否进行链接健康检测(死链、暗链、恶意外链)
    private String malicious_link;

    private String topicUrn;

    public String getAk() {
        return ak;
    }

    @DataBoundSetter
    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }
    @DataBoundSetter
    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getRegion() {
        return region;
    }
    @DataBoundSetter
    public void setRegion(String region) {
        this.region = region;
    }

    public String getUpgrade() {
        return upgrade;
    }
    @DataBoundSetter
    public void setUpgrade(String upgrade) {
        this.upgrade = upgrade;
    }

    public String getTask_name() {
        return task_name;
    }
    @DataBoundSetter
    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getUrl() {
        return url;
    }
    @DataBoundSetter
    public void setUrl(String url) {
        this.url = url;
    }

    public String getTask_type() {
        return task_type;
    }
    @DataBoundSetter
    public void setTask_type(String task_type) {
        this.task_type = task_type;
    }

    public String getTimer() {
        return timer;
    }
    @DataBoundSetter
    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getTrigger_time() {
        return trigger_time;
    }
    @DataBoundSetter
    public void setTrigger_time(String trigger_time) {
        this.trigger_time = trigger_time;
    }

    public String getTask_period() {
        return task_period;
    }
    @DataBoundSetter
    public void setTask_period(String task_period) {
        this.task_period = task_period;
    }

    public String getScan_mode() {
        return scan_mode;
    }
    @DataBoundSetter
    public void setScan_mode(String scan_mode) {
        this.scan_mode = scan_mode;
    }

    public String getPort_scan() {
        return port_scan;
    }
    @DataBoundSetter
    public void setPort_scan(String port_scan) {
        this.port_scan = port_scan;
    }

    public String getWeak_pwd_scan() {
        return weak_pwd_scan;
    }
    @DataBoundSetter
    public void setWeak_pwd_scan(String weak_pwd_scan) {
        this.weak_pwd_scan = weak_pwd_scan;
    }

    public String getCve_check() {
        return cve_check;
    }
    @DataBoundSetter
    public void setCve_check(String cve_check) {
        this.cve_check = cve_check;
    }

    public String getText_check() {
        return text_check;
    }
    @DataBoundSetter
    public void setText_check(String text_check) {
        this.text_check = text_check;
    }

    public String getPicture_check() {
        return picture_check;
    }
    @DataBoundSetter
    public void setPicture_check(String picture_check) {
        this.picture_check = picture_check;
    }

    public String getMalicious_code() {
        return malicious_code;
    }
    @DataBoundSetter
    public void setMalicious_code(String malicious_code) {
        this.malicious_code = malicious_code;
    }

    public String getMalicious_link() {
        return malicious_link;
    }
    @DataBoundSetter
    public void setMalicious_link(String malicious_link) {
        this.malicious_link = malicious_link;
    }

    public String getTopicUrn() {
        return topicUrn;
    }

    @DataBoundSetter
    public void setTopicUrn(String topicUrn) {
        this.topicUrn = topicUrn;
    }

    @DataBoundConstructor
    public vssCheckPipelineStep(String region) {
        this.region = region;
    }

    @Override
    public StepExecution start(StepContext context) throws Exception {
        return new vssCheckPipelineStep.Execution(this, context);
    }

    @Extension
    public static class DescriptorImpl extends StepDescriptor {

        @Override
        public Set<? extends Class<?>> getRequiredContext() {
            return requires(TaskListener.class, EnvVars.class, FilePath.class);
        }

        @Override
        public String getFunctionName() {
            return "VssPipeline";
        }

        @Override
        public String getDisplayName() {
            return "华为云漏洞扫描";
        }
    }

    public static <T extends Class<?>> Set<T> requires(T... classes) {
        return new HashSet<>(Arrays.asList(classes));
    }

    public static class Execution extends SynchronousNonBlockingStepExecution<String> {

        protected static final long serialVersionUID = 1L;

        protected final transient vssCheckPipelineStep step;

        public Execution(vssCheckPipelineStep step, StepContext context) {
            super(context);
            this.step = step;

        }

        @Override
        public String run() throws Exception {
            CustomInput customInput = new CustomInput();
            customInput.setAk(step.getAk());
            customInput.setSk(step.getSk());
            customInput.setRegion(step.getRegion());
            customInput.setUpgrade(step.getUpgrade());
            customInput.setTask_name(step.getTask_name());
            customInput.setUrl(step.getUrl());
            customInput.setTask_type(step.getTask_type());
            customInput.setTrigger_time(step.getTrigger_time());
            customInput.setTask_period(step.getTask_period());
            customInput.setScan_mode(step.getScan_mode());
            customInput.setPort_scan(step.getPort_scan());
            customInput.setWeak_pwd_scan(step.getWeak_pwd_scan());
            customInput.setCve_check(step.getCve_check());
            customInput.setText_check(step.getText_check());
            customInput.setPicture_check(step.getPicture_check());
            customInput.setMalicious_code(step.getMalicious_code());
            customInput.setMalicious_link(step.getMalicious_link());
            customInput.setTopicUrn(step.getTopicUrn());
            TaskListener listener = vssCheckPipelineStep.Execution.this.getContext().get(TaskListener.class);
            return vssService.CreateTasks(listener, customInput);
        }
    }
}
